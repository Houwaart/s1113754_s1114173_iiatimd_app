package com.appofholding.app;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.muddzdev.styleabletoast.StyleableToast;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static final int ADD_CHARACTER_REQUEST = 1;
    public static final int EDIT_CHARACTER_REQUEST = 2;

    public final String firebaseToken = "dTvXJ_BYyZw:APA91bEtSR1nfNwsbGK08Wp6-nVMiPI86feURmxF3275cVxRx_ITHC1U-SNVWCdshqgJg-sZ4tFkZOqaiEmp_rgJrV5BdY3R3FMiZ60JH_vo3QyIb1B_leNLoKgLuUqV7JrBd_x36-sU";
    private final String TAG = "test";

    private CharacterViewModel characterViewModel;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, new OnSuccessListener<InstanceIdResult>() {
//            @Override
//            public void onSuccess(InstanceIdResult instanceIdResult) {
//                String token = instanceIdResult.getToken();
//                Log.d("FCM Token", token);
//            }
//        });

        FloatingActionButton buttonAddCharacter = findViewById(R.id.button_add_character);
        buttonAddCharacter.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MainActivity.this, AddCharacterActivity.class);
                startActivityForResult(intent, ADD_CHARACTER_REQUEST);
            }
        });

        RecyclerView recyclerView = findViewById(R.id.characterRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.hasFixedSize();

        final CharacterAdapter adapter = new CharacterAdapter();
        recyclerView.setAdapter(adapter);


        characterViewModel = new ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory.getInstance(this.getApplication())).get(CharacterViewModel.class);
        characterViewModel.getAllCharacters().observe(this, new Observer<List<Character>>() {
            @Override
            public void onChanged(List<Character> characters) {
                Log.d("gelukt",String.valueOf(characters));
                adapter.submitList(characters);
            }
        });

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                characterViewModel.delete(adapter.getCharacterAt(viewHolder.getAdapterPosition()));
                StyleableToast.makeText(MainActivity.this, "Character deleted", R.style.characterWarnToast).show();
            }
        }).attachToRecyclerView(recyclerView);


        adapter.setOnCharacterClickListener(new CharacterAdapter.onCharacterClickListener() {
            @Override
            public void onCharacterClick(Character character) {
                Intent intent = new Intent(MainActivity.this, AddCharacterActivity.class);
                intent.putExtra(AddCharacterActivity.EXTRA_CHARACTER_ID, character.getCharacterId());
                intent.putExtra(AddCharacterActivity.EXTRA_NAME, character.getName());
                intent.putExtra(AddCharacterActivity.EXTRA_DESCRIPTION, character.getDescription());
                startActivityForResult(intent, EDIT_CHARACTER_REQUEST);
            }
        });



        createNotificationChannel();
    }

    public void createNotificationChannel() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "DeletedCharactersChannel";
            String description = "Channel for broadcasting characters deletion";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("notifyCharactersDeletion",name,importance);
            channel.setDescription(description);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



        if(requestCode == ADD_CHARACTER_REQUEST && resultCode == RESULT_OK){
            String name = data.getStringExtra(AddCharacterActivity.EXTRA_NAME);
            String description = data.getStringExtra(AddCharacterActivity.EXTRA_DESCRIPTION);

            Character character = new Character(name, description);
            characterViewModel.insert(character);

            StyleableToast.makeText(this, "Character Saved", R.style.characterUpdateToast).show();

        } else if(requestCode == EDIT_CHARACTER_REQUEST && resultCode == RESULT_OK){
            int id = data.getIntExtra(AddCharacterActivity.EXTRA_CHARACTER_ID, -1);

            if(id == -1){
                StyleableToast.makeText(this, "Character can't be updated", R.style.characterWarnToast);
                return;
            }



            String name = data.getStringExtra(AddCharacterActivity.EXTRA_NAME);
            String description = data.getStringExtra(AddCharacterActivity.EXTRA_DESCRIPTION);

            Character character = new Character(name, description);
            character.setCharacterId(id);

            characterViewModel.update(character);


            StyleableToast.makeText(this, "Character Updated", R.style.characterUpdateToast).show();

        } else {
            StyleableToast.makeText(this, "Character not Saved", R.style.characterWarnToast).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.delete_all_characters:
                characterViewModel.deleteAllCharacters();
                Intent intent = new Intent(MainActivity.this,Broadcast.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, intent, 0);

                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

                alarmManager.set(AlarmManager.RTC_WAKEUP,0,pendingIntent);

                StyleableToast.makeText(this, "All characters deleted", R.style.characterWarnToast);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}