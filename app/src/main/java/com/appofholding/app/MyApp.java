package com.appofholding.app;

import android.app.Application;

public class MyApp extends Application {
    static MyApp myAppInstance;

    public MyApp() {
        myAppInstance = this;
    }


    public static MyApp getInstance() {
        return myAppInstance;
    }

}
