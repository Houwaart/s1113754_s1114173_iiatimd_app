package com.appofholding.app;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class CharacterHomepageItemAdapter extends RecyclerView.Adapter<CharacterHomepageItemAdapter.CharacterHomepageViewHolder> {
    private ArrayList<CharacterItem> userItems;
    public List<CharacterItem> items = new ArrayList<>();


    private int characterId;
    private Context context;
    public CharacterHomepageItemAdapter(int characterId, Context ctx) {
        this.characterId = characterId;
        this.context = ctx;
    }

    public int getCharacterId() {
        return characterId;
    }

    public static class CharacterHomepageViewHolder extends RecyclerView.ViewHolder{
        public TextView characterHomePageItemName;
        public TextView characterHomePageItemAmount;
        public TextView characterHomePageItemId;
        public TextView characterHomePageBoundToCharacterId;

        public Context context;
        public CharacterHomepageViewHolder(final View v, Context ctx) {
            super(v);
            characterHomePageItemName = v.findViewById(R.id.characterHomePageItemName);
            characterHomePageItemAmount = v.findViewById(R.id.characterHomePageItemAmount);
            characterHomePageItemId = v.findViewById(R.id.characterHomePageItemId);
            characterHomePageBoundToCharacterId = v.findViewById(R.id.characterHomePageBoundToCharacterId);
            this.context = ctx;

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("test","Ik werk CharacterItemAdapter  " + characterHomePageItemId.getText());
                    final FragmentManager manager = ((AppCompatActivity)context).getSupportFragmentManager();
                    CharacterItemViewModel charVM = new CharacterItemViewModel(MyApp.getInstance());
                    ItemBottomSheetDialog bottomSheetDialog = new ItemBottomSheetDialog(String.valueOf(characterHomePageItemName.getText()),String.valueOf(characterHomePageItemAmount.getText()),String.valueOf(characterHomePageBoundToCharacterId.getText()),Integer.valueOf((String) characterHomePageItemId.getText()),"edit");
                    bottomSheetDialog.show(manager,"test");

                }
            });
        }
    }
    @NonNull
    @Override
    public CharacterHomepageItemAdapter.CharacterHomepageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.character_homepage_item, parent, false);
        CharacterHomepageViewHolder characterHomepageViewHolder = new CharacterHomepageViewHolder(v,context);

        return characterHomepageViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CharacterHomepageViewHolder holder, int position) {
        holder.characterHomePageItemName.setText(items.get(position).getItemName());
        holder.characterHomePageItemAmount.setText(String.valueOf(items.get(position).getItemAmount()));
        holder.characterHomePageItemId.setText(String.valueOf(items.get(position).getItemId()));
        holder.characterHomePageBoundToCharacterId.setText(String.valueOf(characterId));

    }
    public void setItems(List<CharacterItem> items) {
        this.items = items;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return items.size();
    }
}
