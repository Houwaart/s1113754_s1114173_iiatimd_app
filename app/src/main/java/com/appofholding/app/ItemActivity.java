package com.appofholding.app;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ItemActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView itemRecyclerView;
    private RecyclerView.Adapter itemRecyclerViewAdapter;
    private RecyclerView.LayoutManager itemLayoutManager;

    private ArrayList<Item> dbItems = new ArrayList<Item>();
    private JSONObject dbData;
    private ItemViewModel itemViewModel;

    public boolean isSorted = true;

    @Override
    protected void onCreate(Bundle savedInstances) {
        super.onCreate(savedInstances);
        setContentView(R.layout.activity_items);

        Button toMainScreenButton = findViewById(R.id.itemsToMainButton);
        toMainScreenButton.setOnClickListener(this);

        RecyclerView recyclerView1 = findViewById(R.id.itemRecyclerView);
        recyclerView1.setLayoutManager(new LinearLayoutManager(this));
        recyclerView1.setHasFixedSize(true);

        String characterId = getIntent().getStringExtra("characterId");
        final ItemAdapter adapter1 = new ItemAdapter(this, characterId);
        recyclerView1.setAdapter(adapter1);

        itemViewModel = ViewModelProviders.of(this).get(ItemViewModel.class);
        itemViewModel.getAllItems().observe(this, new Observer<List<Item>>() {
            @Override
            public void onChanged(final List<Item> items) {

                    Collections.sort(items, new Comparator<Item>() {
                        @Override
                        public int compare(Item o1, Item o2) {
                            return o1.getName().compareToIgnoreCase(o2.getName());
                        }
                    });

                adapter1.setItems(items);
            }

        });



    }
    @Override
    public void onClick(View view) {
        finish();
    }
}
