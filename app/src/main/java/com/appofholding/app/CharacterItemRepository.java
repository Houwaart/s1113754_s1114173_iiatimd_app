package com.appofholding.app;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;


import java.util.List;

public class CharacterItemRepository {
    private CharacterItemDAO characterItemDAO;
    private LiveData<List<CharacterItem>> allCharacterItems;
    private LiveData<List<CharacterItem>> allCharacterItemsFromCharacter;
    private int characterID;

    public CharacterItemRepository(Application application) {
        CharacterDatabase database = CharacterDatabase.getInstance(application);
        characterItemDAO = database.characterItemDAO();
        allCharacterItems = characterItemDAO.getAll();
    }

    public void insert(CharacterItem characterItem){
        new InsertCharacterItemAsyncTask(characterItemDAO).execute(characterItem);
    }

    public void update(CharacterItem characterItem) {
        new UpdateCharacterItemAsyncTask(characterItemDAO).execute(characterItem);
    }

    public void delete(CharacterItem characterItem) {
        new DeleteCharacterItemAsyncTask(characterItemDAO).execute(characterItem);
    }

    public void deleteAllCharacterItems() {
        new DeleteAllCharacterItemAsyncTask(characterItemDAO).execute();
    }

    public void updateItemAmount(int itemAmount, int characterID, int itemId) {
        new UpdateItemAmountAsyncTask(characterItemDAO,itemAmount,characterID,itemId).execute();
    }

    public void deleteItemFromCharacter(int characterID, int itemId) {
        new DeleteItemFromCharacterAsyncTask(characterItemDAO, characterID, itemId).execute();
    }


    public LiveData<List<CharacterItem>> getAllCharacterItems() {
     return allCharacterItems;
    }
    public LiveData<List<CharacterItem>> getAllCharacterItemsFromCharacter(int id) {
        allCharacterItemsFromCharacter = characterItemDAO.getItemsFromCharacter(id);
        return allCharacterItemsFromCharacter;
    }
    public void setCharacterID(int id) {this.characterID = id;}
    private static class DeleteItemFromCharacterAsyncTask extends AsyncTask<Void, Void ,Void> {
        private CharacterItemDAO characterItemDAO;
        private int characterId;
        private int itemId;

        private DeleteItemFromCharacterAsyncTask(CharacterItemDAO characterItemDAO, int characterId, int itemId) {
            this.characterItemDAO = characterItemDAO;
            this.characterId = characterId;
            this.itemId = itemId;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            characterItemDAO.deleteItemFromCharacter(characterId,itemId);
            return null;
        }
    }
    private static class UpdateItemAmountAsyncTask extends AsyncTask<Void, Void ,Void> {
        private CharacterItemDAO characterItemDAO;
        private int itemAmount;
        private int characterId;
        private int itemId;

        private UpdateItemAmountAsyncTask(CharacterItemDAO characterItemDAO, int itemAmount, int characterId, int itemId) {
            this.characterItemDAO = characterItemDAO;
            this.itemAmount = itemAmount;
            this.characterId = characterId;
            this.itemId = itemId;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            characterItemDAO.updateItemAmount(itemAmount,characterId,itemId);
            return null;
        }
    }

    private static class InsertCharacterItemAsyncTask extends AsyncTask<CharacterItem, Void ,Void> {
        private CharacterItemDAO characterItemDAO;

        private InsertCharacterItemAsyncTask(CharacterItemDAO characterItemDAO) {
            this.characterItemDAO = characterItemDAO;
        }
        @Override
        protected Void doInBackground(CharacterItem... characterItems) {
            characterItemDAO.insert(characterItems[0]);
            return null;
        }
    }

    private static class UpdateCharacterItemAsyncTask extends AsyncTask<CharacterItem, Void ,Void> {
        private CharacterItemDAO characterItemDAO;

        private UpdateCharacterItemAsyncTask(CharacterItemDAO characterItemDAO) {
            this.characterItemDAO = characterItemDAO;
        }
        @Override
        protected Void doInBackground(CharacterItem... characterItems) {
            characterItemDAO.update(characterItems[0]);
            return null;
        }
    }

    private static class DeleteCharacterItemAsyncTask extends AsyncTask<CharacterItem, Void ,Void> {
        private CharacterItemDAO characterItemDAO;

        private DeleteCharacterItemAsyncTask(CharacterItemDAO characterItemDAO) {
            this.characterItemDAO = characterItemDAO;
        }
        @Override
        protected Void doInBackground(CharacterItem... characterItems) {
            characterItemDAO.delete(characterItems[0]);
            return null;
        }
    }

    private static class DeleteAllCharacterItemAsyncTask extends AsyncTask<Void, Void ,Void> {
        private CharacterItemDAO characterItemDAO;

        private DeleteAllCharacterItemAsyncTask(CharacterItemDAO characterItemDAO) {
            this.characterItemDAO = characterItemDAO;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            characterItemDAO.deleteAllCharacterItems();
            return null;
        }
    }

}
