package com.appofholding.app;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.muddzdev.styleabletoast.StyleableToast;

import java.util.List;

public class CharacterHomepageActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView itemRecyclerView;
    private RecyclerView.Adapter itemRecyclerViewAdapter;
    private RecyclerView.LayoutManager itemLayoutManager;

    private FloatingActionButton toItemActivity;

    private CharacterItemViewModel characterItemViewModel;
    private String characterIds;
    @Override
    protected void onCreate(Bundle savedInstances) {
        super.onCreate(savedInstances);
        setContentView(R.layout.activity_character_homepage);
        String data = getIntent().getStringExtra("name");
        Log.d("test",String.valueOf(getIntent().getStringExtra("characterId")));
        this.characterIds = String.valueOf(getIntent().getStringExtra("characterId"));
        final Integer characterId = Integer.valueOf(getIntent().getStringExtra("characterId"));
        TextView textView = (TextView)findViewById(R.id.characterHomepageName);
        textView.setText(data);

//        Item buttonAdd = findViewById(R.id.add_custom_character_item);
//        buttonAdd.setOnClickListener(new View.OnClickListener(){
//
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(CharacterHomepageActivity.this,AddCustomCharacterItemActivity.class);
//                startActivity(intent);
//            }
//        });

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_arrow);


        toItemActivity = findViewById(R.id.character_item_button);
        toItemActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent i = new Intent(context,ItemActivity.class);
                i.putExtra("characterId",getIntent().getStringExtra("characterId"));
                context.startActivity(i);
            }
        });

        itemRecyclerView = findViewById(R.id.characterHomepageRecyclerView);
        itemLayoutManager = new LinearLayoutManager(this);
        itemRecyclerView.setLayoutManager(itemLayoutManager);
        itemRecyclerView.hasFixedSize();


        final CharacterHomepageItemAdapter adapter = new CharacterHomepageItemAdapter(characterId,this);
        itemRecyclerView.setAdapter(adapter);

        characterItemViewModel = new ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory.getInstance(this.getApplication())).get(CharacterItemViewModel.class);
        characterItemViewModel.setCharacterID(characterId);

        characterItemViewModel.getAllCharacterItemsFromCharacter(characterId).observe(this, new Observer<List<CharacterItem>>() {
            @Override
            public void onChanged(List<CharacterItem> characterItems) {
                adapter.setItems(characterItems);

            }
        });

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                StyleableToast.makeText(CharacterHomepageActivity.this, "Character deleted", R.style.characterWarnToast).show();
            }
        });


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.custom_item_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.add_custom_character_item:
                Intent intent = new Intent(CharacterHomepageActivity.this,AddCustomCharacterItemActivity.class);
                intent.putExtra("characterId",characterIds);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {

    }
}
