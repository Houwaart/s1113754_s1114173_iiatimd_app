package com.appofholding.app;

public class InsertCharacterItemTask implements Runnable{

    CharacterDatabase db;
    CharacterItem characterItem;

    public InsertCharacterItemTask(CharacterDatabase db, CharacterItem characterItem) {
        this.db = db;
        this.characterItem = characterItem;
    }

    @Override
    public void run() {
        db.characterItemDAO().insert(this.characterItem);
    }
}
