package com.appofholding.app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.appofholding.app.R;
import com.muddzdev.styleabletoast.StyleableToast;

public class AddCharacterActivity extends AppCompatActivity {
    public static final String EXTRA_NAME =
            "com.example.app_of_holding.EXTRA_NAME";
    public static final String EXTRA_DESCRIPTION =
            "com.example.app_of_holding.EXTRA_DESCRIPTION";
    public static final String EXTRA_CHARACTER_ID =
            "com.example.app_of_holding.EXTRA_CHARACTER_ID";

    private EditText editTextName;
    private EditText editTextDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_character);

        editTextName = findViewById(R.id.edit_text_name);
        editTextDescription = findViewById(R.id.edit_text_description);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        Intent intent = getIntent();

        if(intent.hasExtra(EXTRA_CHARACTER_ID)){
            setTitle("Edit Character");
            editTextName.setText(intent.getStringExtra(EXTRA_NAME));
            editTextDescription.setText(intent.getStringExtra(EXTRA_DESCRIPTION));
        } else {
            setTitle("Add Character");
        }
    }

    private void saveCharacter(){
        String name = editTextName.getText().toString();
        String description = editTextDescription.getText().toString();

        if(name.trim().isEmpty() || description.trim().isEmpty()){
            StyleableToast.makeText(this, "Please insert a name and description", R.style.characterWarnToast);
            return;
        }

        Intent data = new Intent();
        data.putExtra(EXTRA_NAME, name);
        data.putExtra(EXTRA_DESCRIPTION, description);

        int id = getIntent().getIntExtra(EXTRA_CHARACTER_ID, -1);
        if(id != -1){
            data.putExtra(EXTRA_CHARACTER_ID, id);
        }

        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_character_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_character:
                saveCharacter();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}