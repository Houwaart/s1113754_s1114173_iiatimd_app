package com.appofholding.app;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class ItemRepository {

    private ItemDAO itemDAO;
    private LiveData<List<Item>> allItems;

    public ItemRepository(Application application) {
        CharacterDatabase database = CharacterDatabase.getInstance(application);
        itemDAO = database.itemDao();
        allItems = itemDAO.getAll();
    }

    public void insert(Item item) {
        new InsertItemAsyncTask(itemDAO).execute(item);
    }

    public void update(Item item) {
        new InsertItemAsyncTask(itemDAO).execute(item);
    }

    public void delete(Item item) {
        new InsertItemAsyncTask(itemDAO).execute(item);
    }

    public LiveData<List<Item>> getAllItems() { return allItems;}

    public static class InsertItemAsyncTask extends AsyncTask<Item, Void, Void> {
        private ItemDAO itemDAO;

        private InsertItemAsyncTask(ItemDAO itemDAO){
            this.itemDAO = itemDAO;
        }

        @Override
        protected Void doInBackground(Item... items) {
            itemDAO.insert(items[0]);
            return null;
        }
    }
}
