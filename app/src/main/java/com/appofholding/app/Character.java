package com.appofholding.app;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

// this class creates the table data for characters

@Entity(tableName = "character_table")
public class Character {

    @PrimaryKey(autoGenerate = true)
    private int characterId;

    private String name;

    private String description;

    public Character(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public void setCharacterId(int characterId) {
        this.characterId = characterId;
    }

    public int getCharacterId() {
        return characterId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
