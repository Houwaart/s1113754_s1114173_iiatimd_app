package com.appofholding.app;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class CharacterRepository {
    private CharacterDao characterDao;
    private LiveData<List<Character>> allCharacters;

    public CharacterRepository(Application application){
        CharacterDatabase database = CharacterDatabase.getInstance(application);
        characterDao = database.characterDao();
        allCharacters = characterDao.getAllCharacters();
    }

    public void insert(Character character){
        new InsertCharacterAsyncTask(characterDao).execute(character);
    }

    public void update(Character character){
        new UpdateCharacterAsyncTask(characterDao).execute(character);
    }

    public void delete(Character character){
        new DeleteCharacterAsyncTask(characterDao).execute(character);
    }

    public void deleteAllCharacters(){
        new DeleteAllCharactersAsyncTask(characterDao).execute();
    }

    public LiveData<List<Character>> getAllCharacters(){
        return allCharacters;
    }

    private static class InsertCharacterAsyncTask extends AsyncTask<Character, Void, Void> {
        private CharacterDao characterDao;

        private InsertCharacterAsyncTask(CharacterDao characterDao){
            this.characterDao = characterDao;
        }

        @Override
        protected Void doInBackground(Character... characters) {
            characterDao.insert(characters[0]);
            return null;
        }
    }


    private static class DeleteCharacterAsyncTask extends AsyncTask<Character, Void, Void> {
        private CharacterDao characterDao;

        private DeleteCharacterAsyncTask(CharacterDao characterDao){
            this.characterDao = characterDao;
        }

        @Override
        protected Void doInBackground(Character... characters) {
            characterDao.delete(characters[0]);
            return null;
        }
    }

    private static class UpdateCharacterAsyncTask extends AsyncTask<Character, Void, Void> {
        private CharacterDao characterDao;

        private UpdateCharacterAsyncTask(CharacterDao characterDao){
            this.characterDao = characterDao;
        }

        @Override
        protected Void doInBackground(Character... characters) {
            characterDao.update(characters[0]);
            return null;
        }
    }

    private static class DeleteAllCharactersAsyncTask extends AsyncTask<Void, Void, Void> {
        private CharacterDao characterDao;

        private DeleteAllCharactersAsyncTask(CharacterDao characterDao){
            this.characterDao = characterDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            characterDao.deleteAllCharacters();
            return null;
        }
    }

}
