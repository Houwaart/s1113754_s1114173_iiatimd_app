package com.appofholding.app;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

// this class adds the tables to a Room database
@Database(entities = {Character.class, Item.class, CharacterItem.class}, version = 84)
public abstract class CharacterDatabase extends RoomDatabase {
        private CharacterDao characterDao;
        private ItemDAO itemDAO;
        private CharacterItemDAO characterItemDAO;

    private static CharacterDatabase instance;

    public abstract CharacterDao characterDao();
    public abstract CharacterItemDAO characterItemDAO();
    public abstract ItemDAO itemDao();




    public static synchronized CharacterDatabase getInstance(Context context){
        if(instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    CharacterDatabase.class, "Character_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            Log.d("gelukt","databasie gemaakt");
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }

        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            Log.d("gelukt","Database opened");
            new PopulateDbAsyncTask(instance).execute();
        }
    };


    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {

        private CharacterDao characterDao;
        private CharacterItemDAO characterItemDAO;
        private ItemDAO itemDAO;
        private PopulateDbAsyncTask(CharacterDatabase db){
            characterDao = db.characterDao();
            characterItemDAO = db.characterItemDAO();
            itemDAO = db.itemDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://applepi.nl/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            JsonAPI jsonAPI = retrofit.create(JsonAPI.class);

            Call<List<Item>> call = jsonAPI.getItems();

            call.enqueue(new retrofit2.Callback<List<Item>>() {
                @Override
                public void onResponse(Call<List<Item>> call, retrofit2.Response<List<Item>> response) {
                    Log.d("gelukt", String.valueOf(response));

                    List<Item> items = response.body();
                    Log.d("gelukt", String.valueOf(response.body()));
                    for(Item item : items) {

                        Item insertItem = new Item(item.getName(),item.getId());
                        Thread insert = new Thread(new InsertItemTask(itemDAO,insertItem));
                        insert.start();
                        try {
                            insert.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    }
                }

                @Override
                public void onFailure(Call<List<Item>> call, Throwable t) {
                    Log.d("gefaald", t.getMessage());
                }
            });
            return null;
        }
    }


}
