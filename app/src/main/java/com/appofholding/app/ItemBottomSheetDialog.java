package com.appofholding.app;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.muddzdev.styleabletoast.StyleableToast;

public class ItemBottomSheetDialog extends BottomSheetDialogFragment {

    private BottomSheetListener mListener;
    public String itemName;
    public int itemId;
    public String itemAmount;
    public String characterId;
    public String option;

    public ItemBottomSheetDialog(String name, String amount, String characterId, int itemId, String option) {
        this.itemName = name;
        this.itemAmount = amount;
        this.characterId = characterId;
        this.itemId = itemId;
        this.option = option;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottom_sheet_layout, container, false);
        v.setBackgroundColor(getResources().getColor(R.color.colorBackground));

        Button buttonPlus = v.findViewById(R.id.bottomSheetLayoutButtonPlus);
        Button buttonMin = v.findViewById(R.id.bottomSheetLayoutButtonMin);
        Button buttonConfirm = v.findViewById(R.id.bottomSheetLayoutButtonConfirm);

        TextView textViewItemName = v.findViewById(R.id.bottomSheetLayoutTextItemName);
        final TextView textViewItemAmount = v.findViewById(R.id.bottomSheetLayoutTextItemAmount);
        textViewItemName.setText(this.itemName);
        textViewItemAmount.setText(this.itemAmount);
        final String characterId = this.characterId;
        final int itemId = this.itemId;
        final CharacterItemViewModel[] characterItemViewModel = new CharacterItemViewModel[1];
        buttonPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int amount = Integer.valueOf((String) textViewItemAmount.getText());
                if(amount < 999) {
                    int newValue = amount + 1;
                    textViewItemAmount.setText(String.valueOf(newValue));
                }



            }
        });

        buttonMin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int amount = Integer.valueOf((String) textViewItemAmount.getText());
                switch (option){
                    case "add":
                        if(amount > 1) {
                            int newValue = amount - 1;
                            textViewItemAmount.setText(String.valueOf(newValue));
                        }

                    case "edit":
                        if(amount <= 0) {
                            int newValue = 0;
                            textViewItemAmount.setText(String.valueOf(newValue));
                        } else {
                            int newValue = amount - 1;
                            textViewItemAmount.setText(String.valueOf(newValue));
                        }

                }


            }
        });

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharacterItemViewModel charVM = new CharacterItemViewModel(MyApp.getInstance());
                switch (option) {
                    case "add":
                        CharacterItem item = new CharacterItem(Integer.valueOf(characterId),itemName,Integer.valueOf((String) textViewItemAmount.getText()),itemId);
                        charVM.insert(item);
                        dismiss();
                        StyleableToast.makeText(v.getContext(),"Item(s) added",R.style.characterUpdateToast).show();

                    case "edit":
                        if(Integer.valueOf((String) textViewItemAmount.getText()) == 0) {
                            charVM.deleteItemFromCharacter(Integer.valueOf(characterId),itemId);
                            dismiss();
                            StyleableToast.makeText(v.getContext(),"Item deleted",R.style.characterUpdateToast).show();
                        } else {
                            charVM.updateItemAmount(Integer.valueOf((String) textViewItemAmount.getText()),Integer.valueOf(characterId),itemId);
                            dismiss();
                        }


                }



            }
        });
        return v;
    }

    public interface BottomSheetListener {
        void onButtonClicked(String text);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }
}
