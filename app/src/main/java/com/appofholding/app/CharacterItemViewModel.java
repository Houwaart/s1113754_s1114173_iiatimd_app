package com.appofholding.app;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class CharacterItemViewModel extends AndroidViewModel {
    private CharacterItemRepository repository;
    private LiveData<List<CharacterItem>> allCharacterItems;
    private LiveData<List<CharacterItem>> allCharacterItemsFromCharacter;
    private int characterID;
    public CharacterItemViewModel(@NonNull Application application) {
        super(application);
        repository = new CharacterItemRepository(application);
        allCharacterItems = repository.getAllCharacterItems();
        allCharacterItemsFromCharacter = repository.getAllCharacterItemsFromCharacter(characterID);
    }

    public void insert(CharacterItem characterItem) { repository.insert(characterItem);}
    public void update(CharacterItem characterItem) { repository.update(characterItem);}
    public void delete(CharacterItem characterItem) { repository.delete(characterItem);}
    public void setCharacterID(int id){repository.setCharacterID(id); this.characterID = id;}

    public LiveData<List<CharacterItem>> getAllCharacterItems() {return allCharacterItems;}
    public LiveData<List<CharacterItem>> getAllCharacterItemsFromCharacter(int id) {
        setCharacterID(id);
        Log.d("test", "test viewModel " + String.valueOf(id));
        allCharacterItemsFromCharacter = repository.getAllCharacterItemsFromCharacter(id);
        return allCharacterItemsFromCharacter;
    }

    public void updateItemAmount(int itemAmount, int characterID, int itemId) {
        repository.updateItemAmount(itemAmount,characterID,itemId);
    }

    public void deleteItemFromCharacter(int characterID, int itemId) {
        repository.deleteItemFromCharacter(characterID, itemId);
    }

}
