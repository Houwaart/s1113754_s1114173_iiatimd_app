package com.appofholding.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder> {

    public List<Item> items = new ArrayList<>();
    public Context context;
    public String characterId;
    public ItemAdapter(Context context, String characterId) {
        this.context = context;
        this.characterId = characterId;
    }
    public static class ItemViewHolder extends RecyclerView.ViewHolder implements ItemBottomSheetDialog.BottomSheetListener {

        public TextView itemName;
        public TextView itemID;

        private Context context;
        private String characterId;





        public ItemViewHolder(View v, Context ctx, final String characterId) {
            super(v);
            this.characterId = characterId;
            this.context = ctx;
            itemName = v.findViewById(R.id.itemName);
            itemID = v.findViewById(R.id.itemID);
            final FragmentManager manager = ((AppCompatActivity)context).getSupportFragmentManager();


            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String itemIdString = String.valueOf(itemID.getText());
                    ItemBottomSheetDialog bottomSheetDialog = new ItemBottomSheetDialog((String) itemName.getText(),"1",characterId,Integer.valueOf(itemIdString),"add");
                    bottomSheetDialog.show(manager,"test");
                }
            });
        }

        @Override
        public void onButtonClicked(String text) {
            
        }
    }


    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(v,context,characterId);
        return itemViewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        //Item currentItem = items.get(position);
        holder.itemName.setText(items.get(position).getName());
        holder.itemID.setText(String.valueOf(items.get(position).getId()));
    }

    public void setItems(List<Item> items) {
        this.items = items;
        notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}
