package com.appofholding.app;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.appofholding.app.R;

public class CharacterAdapter extends ListAdapter<Character, CharacterAdapter.CharacterHolder> {
    private onCharacterClickListener listener;

    public CharacterAdapter() {
        super(DIFF_CALLBACK);
    }

    private static final DiffUtil.ItemCallback<Character> DIFF_CALLBACK = new DiffUtil.ItemCallback<Character>() {
        @Override
        public boolean areItemsTheSame(@NonNull Character oldItem, @NonNull Character newItem) {
            return oldItem.getCharacterId() == newItem.getCharacterId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Character oldItem, @NonNull Character newItem) {
            return oldItem.getName().equals(newItem.getName()) &&
                    oldItem.getDescription().equals(newItem.getDescription());
        }
    };

    class CharacterHolder extends RecyclerView.ViewHolder {
        private TextView characterName;
        private TextView characterDescription;
        private TextView characterID;

        public CharacterHolder(@NonNull View itemView) {
            super(itemView);
            characterName = itemView.findViewById(R.id.characterName);
            characterDescription = itemView.findViewById(R.id.characterDescription);
            characterID = itemView.findViewById(R.id.characterId);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {

                        listener.onCharacterClick(getItem(position));
                    }
                    return true;
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    Intent i = new Intent(context,CharacterHomepageActivity.class);
                    i.putExtra("name",characterName.getText());
                    Log.d("test", "char id " + characterID.getText());
                    i.putExtra("characterId", characterID.getText());
                    context.startActivity(i);
                }
            });
        }


    }

    public interface onCharacterClickListener {
        void onCharacterClick(Character character);
    }

    public void setOnCharacterClickListener(onCharacterClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public CharacterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.character_item, parent, false);
        return new CharacterHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CharacterHolder holder, int position) {
        Character currentCharacter = getItem(position);
        holder.characterName.setText(currentCharacter.getName());
        holder.characterDescription.setText(currentCharacter.getDescription());
        holder.characterID.setText(String.valueOf(currentCharacter.getCharacterId()));
    }

    public Character getCharacterAt(int position) {
        return getItem(position);
    }


}
