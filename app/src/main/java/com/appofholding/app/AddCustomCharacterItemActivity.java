package com.appofholding.app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddCustomCharacterItemActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText name;
    private int characterId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_custom_character_item);
        this.characterId = Integer.valueOf(getIntent().getStringExtra("characterId"));
        name = findViewById(R.id.addCustomCharacterItemInput);
        setTitle("Add custom item");

        Button backToCharactersButton = findViewById(R.id.backToCharacterButton);
        backToCharactersButton.setOnClickListener(this);

    }

    private void saveItem() {
        String inputName = name.getText().toString();
        CharacterItemViewModel charVM = new CharacterItemViewModel(MyApp.getInstance());
        if(inputName.trim().isEmpty()) {
            Toast.makeText(this,"Please enter a name", Toast.LENGTH_LONG).show();
            return;
        }
        CharacterItem item = new CharacterItem(characterId,inputName,1,0);
        charVM.insert(item);
        finish();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_custom_character_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_custom_character_item:
                saveItem();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }


    @Override
    public void onClick(View v) {
        finish();
    }
}