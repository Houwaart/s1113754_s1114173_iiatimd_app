package com.appofholding.app;

import java.util.List;

public class GetCharacterItemsFromCharacterIdTask implements Runnable {

    private static List<CharacterItem> items;
    CharacterDatabase db;
    Integer characterId;

    public static List<CharacterItem> getItems(){
        return items;
    }
    public GetCharacterItemsFromCharacterIdTask(CharacterDatabase db, Integer characterId) {
        this.db = db;
        this.characterId = characterId;
    }
    @Override
    public void run() {
        db.characterItemDAO().getItemsFromCharacter(this.characterId);
    }
}
