package com.appofholding.app;

public class InsertItemTask implements Runnable {

    ItemDAO itemDAO;
    Item item;

    public InsertItemTask(ItemDAO itemDAO, Item item) {
        this.itemDAO = itemDAO;
        this.item = item;
    }
    @Override
    public void run() {
        itemDAO.insert(item);
    }
}
