package com.appofholding.app;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonAPI {
    @GET("items")
    Call<List<Item>> getItems();


}
