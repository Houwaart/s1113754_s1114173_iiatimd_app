package com.appofholding.app;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "character_item_table")
public class CharacterItem {

    @ColumnInfo
    private int boundToCharacterId;
    private int itemAmount;
    private String itemName;

    @PrimaryKey(autoGenerate = true)
    private int itemId;





    public CharacterItem(int boundToCharacterId, String itemName, int itemAmount, @Nullable int itemId) {
        this.boundToCharacterId = boundToCharacterId;
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemAmount = itemAmount;
    }

    public int getItemId() {
        return this.itemId;
    }

    public String getItemName() {
        return this.itemName;
    }

    public int getItemAmount() { return this.itemAmount; }

    public int getBoundToCharacterId() {
        return this.boundToCharacterId;
    }
}
