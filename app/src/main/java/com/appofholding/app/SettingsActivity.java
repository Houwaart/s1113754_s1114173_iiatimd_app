package com.appofholding.app;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstances){
        super.onCreate(savedInstances);
        setContentView(R.layout.activity_settings);

        Button toHomeScreenButton = findViewById(R.id.toHomeButton);
        toHomeScreenButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent toHomeScreenIntent = new Intent(this, MainActivity.class);
        startActivity(toHomeScreenIntent);
    }
}
