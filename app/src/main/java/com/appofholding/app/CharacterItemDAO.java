package com.appofholding.app;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface CharacterItemDAO {

    @Query("SELECT * FROM character_item_table")
    LiveData<List<CharacterItem>> getAll();

    @Query("SELECT * FROM character_item_table WHERE boundToCharacterId = :characterId")
    LiveData<List<CharacterItem>> getItemsFromCharacter(int characterId);

    @Query("UPDATE character_item_table SET itemAmount = :itemAmount WHERE boundToCharacterId = :characterId AND itemId = :itemId")
    void updateItemAmount(int itemAmount, int characterId, int itemId);

    @Query("DELETE FROM character_item_table WHERE boundToCharacterId = :characterId AND itemId = :itemId")
    void deleteItemFromCharacter(int characterId, int itemId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(CharacterItem characterItem);

    @Update
    void update(CharacterItem characterItem);

    @Delete
    void delete(CharacterItem characterItem);

    @Query("DELETE FROM character_item_table")
    void deleteAllCharacterItems();
}
